<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <%--  importar las etiquetas de JSTL y usaras en .jsp para llamar a forErch  --%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%> <%--  Librerias para llamar imagenes  archivos estaticos  --%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%> <%--  Libreria para cambiar formato de fecha--%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>Welcome a CineApp</title>
	<spring:url value="/resources" var="urlPublic"/>
	<link rel="stylesheet" href="${urlPublic}/bootstrap/css/bootstrap.min.css">
	
</head>
<body>
<%-- Con esto se trae la ruta de resources	${urlPublic} --%>
	<h1>Welcome to the Home Page</h1>
	<!-- 	Codigo dinamico -->
	<!-- 	<ol> -->
	<%-- 		<c:forEach items="${ peliculas }" var="peliculas"> --%>
	<%-- 			<li>${peliculas}</li> --%>
	<%-- 		</c:forEach> --%>
	<!-- 	</ol> -->

	<!-- Tabla de Peliculas -->

	<div class="panel panel-default"> <%-- class="card" --%>
		<div class="panel-heading">
			<h3>Tabla de Peliculas</h3>
		</div>
		<div class="panel-body">
			<blockquote class="blockquote mb-0">
				<table class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>Id</th>
							<th>Titulo</th>
							<th>Duración</th>
							<th>Clasificación</th>
							<th>Genero</th>
							<th>Imagen</th>
							<th>Fecha Estreno</th>
							<th>Estatus</th>

						</tr>
					</thead>
					<tbody>
						<c:forEach items="${ peliculas }" var="peliculas">
							<tr>
								<td>${ peliculas.id }</td>
								<td>${ peliculas.titulo }</td>
								<td>${ peliculas.duracion } min.</td>
								<td>${ peliculas.clasificacion }</td>
								<td>${ peliculas.genero }</td>
								<td><img src="${urlPublic}/images/${peliculas.imagen}" width="80" height="100"></td>
								<td><fmt:formatDate value="${ peliculas.fechaEstreno }" pattern="dd-MM-yyyy"/> </td>
								<%--<td>${ peliculas.estatus }</td> --%>
								<td>
									<c:choose> <%-- Condicionales if y else --%>
										<c:when test="${ peliculas.estatus=='Activa' }">
											<span class="label label-success">ACTIVA</span> <%-- class="badge badge-success" --%>
										</c:when>
										<c:otherwise>
											<span class="label label-danger">INACTIVA</span> <%--class="badge badge-danger" --%>
										</c:otherwise>
									</c:choose>				
								</td>

							</tr>
						</c:forEach>
					</tbody>

				</table>

			</blockquote>
		</div>
	</div>





</body>
</html>